function cargarDatos(){

    axios
    .get("./html/alumnos.json")
    .then((res) =>{
        mostrar(res.data); 
        const btnCargarDatos = document.getElementById('btnCargarDatos');
        btnCargarDatos.disabled = true; 
    }).catch((err)=>{
        console.log("Surgio un error" + err);
    })

    const res = document.getElementById('tabla');
    const resProGen = document.getElementById('promedioGeneral');

    var PromedioGen = 0;
    const mostrar=(data)=>{
        
        for(let item of data){

            const mate = item.matematicas ;
            const quimica = item.quimica ; 
            const fisica = item.fisica ; 
            const geografia = item.geografia ;

            const promedio = (mate + quimica + fisica + geografia)/4;
            PromedioGen = PromedioGen + promedio;
 
            res.innerHTML += "<tr class='resultados'><td>" + item.id + "</td>" +
            "<td>" + item.matricula + "</td>" +
            "<td>" + item.nombre + "</td>" +
            "<td>" + item.matematicas + "</td>" +
            "<td>" + item.quimica + "</td>" +
            "<td>" + item.fisica + "</td>" +
            "<td>" + item.geografia + "</td>" +
            "<td>" + promedio + "</td><tr>";
            

        }

        resProGen.innerHTML = "Promedio General: " + PromedioGen/18;
        }
}

function Limpiar(){
    location. reload()
}
